/*
* @file Numbers.cpp
* ������ ������. � ������ ����� ������� �� �������. ������� "Armstrong_numbers()", �� ���������
* ����� ���������� � �������� ��������. �� ������� "chose_option()", ��� ������ �������� 䳿.
*/
#define _CRT_SECURE_NO_WARNINGS
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "Numbers.h"

void Armstrong_numbers(int low, int high) {
	int number, origlNum, remainder, count = 0;
	double result = 0.0;
	for (number = low; number < high + 1; ++number) {
		origlNum = number;
		while (origlNum != 0) {
			origlNum /= 10;
			++count;
		}
		origlNum = number;
		while (origlNum != 0) {
			remainder = origlNum % 10;
			result += pow(remainder, count);
			origlNum /= 10;
		}
		if ((int)result == number) {
			printf("\'%d\' ", number);
		}
		count = 0;
		result = 0;
	}
}

void chose_option(int chose) {
	int flag = 0;
	char buffer[256];
label:
	printf("\nPRESS \'1\' TO RESTART PROGRAMM:\n");
	printf("PRESS \'0\' TO END:\n");
	printf("-> ");
	while (true) {
		flag = 1;
		scanf("%s", buffer);
		for (int i = 0; i < strlen(buffer); i++) {
			if (!isdigit(buffer[i])) {
				printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
				printf("-> ");
				flag = 0;
				break;
			}
		}
		if (flag) {
			chose = atoi(buffer);
			break;
		}
	}
	switch (chose) {
	case 0:
		system("cls");
		printf("THANK YOU FOR WORKING!!!\n");
		exit(0);
		break;
	case 1: break;
	default: goto label;
	}
}