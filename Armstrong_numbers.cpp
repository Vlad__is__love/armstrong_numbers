/*
* @file Armstrong.cpp
* ������ ������. � ������ ����� ������� ������� main.
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <locale.h>
#include "Numbers.h"

int main() {
	setlocale(LC_ALL, "Rus");
	printf(
		"+---------------------------------------------------------------------"
		"-----------------+\n");
	printf(
		"|  ARMSTRONG NUMBERS IN INPUTED DIAPASON                              "
		"                 |\n");
	printf(
		"|  DISCRIPTION: This program shows all Armstrong numbers in inputed "
		"diapason.          |\n");
	printf(
		"|  FORMULA: abcd = a^n + b^n + c^n + d^n, where \'n\' quantity of "
		"digits in number.      |\n");
	printf(
		"+---------------------------------------------------------------------"
		"-----------------+\n");
	while (true) {
		int low = 0, high = 0, chose = 0;
		int flag = 0;
		char buffer[256];
		printf("\nENTER DIAPASON:\n");

		printf("FROM: ");
		while (true) {
			flag = 1;
			scanf("%s", buffer);
			for (int i = 0; i < strlen(buffer); i++) {
				if (!isdigit(buffer[i])) {
					printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
					printf("-> ");
					flag = 0;
					break;
				}
			}
			if (flag) {
				low = atoi(buffer);
				break;
			}
		}
		printf("TO: ");
		while (true) {
			flag = 1;
			scanf("%s", buffer);
			for (int i = 0; i < strlen(buffer); i++) {
				if (!isdigit(buffer[i])) {
					printf("\nINCORECT INPUT!!! TRY AGAIN!!!\n");
					printf("-> ");
					flag = 0;
					break;
				}
			}
			if (flag) {
				high = atoi(buffer);
				break;
			}
		}
		if (low >= high) {
			printf("\nINCORRECT DIAPASON!!! TRY AGAIN!!!\n");
			continue;
		}
		printf("ARMSTRONG NUMBERS BETWEEN \'%d\' AND \'%d\' ARE: ", low, high);
		Armstrong_numbers(low, high);
		printf("\n");
		chose_option(chose);
	}
}